package childs;
import parents.Person;

public class Programmer extends Person {
	private String tecknology;
	
	public Programmer() {
		
	}
	
	public Programmer(String name, String addrest, String tecknology) {
		super(name, addrest);
		this.tecknology = tecknology;
	}

	public void hacking() {
		System.out.println("I can hacking a website");
	}
	
	public void coding() {
		System.out.println("i can coding "+getTecknology()+".");
	}
	
	public void greeting() {
		super.greeting();
		System.out.println("My job is a "+getTecknology()+"programmer.");
	}

	public String getTecknology() {
		return tecknology;
	}

	public void setTecknology(String tecknology) {
		this.tecknology = tecknology;
	}
}
