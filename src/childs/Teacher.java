package childs;
import parents.Person;

public class Teacher extends Person {
	private String subject;
	
	public Teacher(){
		
	}
	
	public Teacher(String name, String addrest, String subject) {
		super(name, addrest);
		this.subject = subject;
	}

	public void teaching() {
		System.out.println("I can teact "+getSubject()+".");
	}
	
	public void greeting() {
		super.greeting();
		System.out.println("My job is a "+getSubject()+".");
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}
	
	
}
