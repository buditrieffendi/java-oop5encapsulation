package childs;
import parents.Person;

public class Doctor extends Person {
	private String specialist;
	
	public Doctor() {
		
	}
	
	public Doctor(String name, String addrest, String specialist) {
		super(name, addrest);
		this.specialist = specialist;
	}

	public void surgery() {
		System.out.println("I can surgary operation patients");
	}

	public void greeting() {
		super.greeting();
		System.out.println("My occupation is a"+getSpecialist()+" doctor");
	}

	public String getSpecialist() {
		return specialist;
	}

	public void setSpecialist(String specialist) {
		this.specialist = specialist;
	}
}
