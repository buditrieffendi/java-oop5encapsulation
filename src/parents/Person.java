package parents;

public class Person {
	private String name;
	private String addrest;
	
	public Person() {
		
	}
	
	public Person(String name, String addrest) {
		super();
		this.name = name;
		this.addrest = addrest;
	}
	
	public void greeting() {
		System.out.println("Helo my name "+ getName() +".");
		System.out.println("I, come from "+ getAddrest() +".");
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAddrest() {
		return addrest;
	}

	public void setAddrest(String addrest) {
		this.addrest = addrest;
	}
	
	
}
