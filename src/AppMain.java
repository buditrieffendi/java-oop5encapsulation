import childs.Doctor;
import childs.Programmer;
import childs.Teacher;
import parents.Person;

public class AppMain {
	public static void main(String[] args) {
		
		//menggunakan parameter
		Person person1 = new Programmer("Jaja","Hilir","Java");
		Person person2 = new Teacher("Joko","Tegal","Matematika");
		Person person3 = new Doctor("Eko","Surabaya","Pedistrician");
		
		//menggunakan contractor default
		Person person4 = new Programmer();
		person4.setName("Ajang");
		person4.setAddrest("Lombok");
		((Programmer)person4).setTecknology("Golang");
		
		System.out.println(((Programmer)person1).getTecknology());
		System.out.println();
		
		sayHello(person1);
		sayHello(person2);
		sayHello(person3);
		sayHello(person4);
		
	}
	static void sayHello(Person person) {
		String msg;
		if(person instanceof Programmer) {
			Programmer programmer = (Programmer) person;
			msg = "Hello,"+programmer.getName()+". Seorang programmer "+programmer.getTecknology()+".";
		}else if(person instanceof Doctor) {
			Doctor doctor = (Doctor) person;
			msg = "Hello,"+doctor.getName()+". Seorang doctor "+doctor.getSpecialist()+".";
		}else if(person instanceof Teacher) {
			Teacher teacher = (Teacher) person;
			msg = "Hello,"+teacher.getName()+". Seorang teacher "+teacher.getSubject()+".";
		}else {
			msg = "Helo, "+person.getName()+".";
		}
		System.out.println(msg);
		System.out.println();
	}
}
